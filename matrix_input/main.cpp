#include "mmio.h"



int main(int argc, char *argv[]){

	//return graph in dense matrix
	std::vector<std::vector<int> > graph_matrix = mtx_to_matrix(argc, argv);

    // print dense graph matrix
    for ( const std::vector<int> &v : graph_matrix ){
        for ( int x : v ){
            std::cout << x << ' ';
        }
        std::cout << std::endl;
    }

	//return graph in adjacency list
	Graph<int> graph_list =  mtx_to_graph(argc, argv);
    //Graph<double> graph_list =  mtx_to_graph_double(argc, argv);
    graph_list.printGraph();



	return 0;	
}

