#ifndef CLASSMAPGRAPH_H
#define CLASSMAPGRAPH_H

#include <iostream>
#include <map>
#include <assert.h>
#include <algorithm>

using namespace std;

/*template <typename T>
struct helperEdge {
    int src;
    int dest;
    T weight;

    void helperPrint(){
        std::cout << "(" << src << "," << dest << "," << weight << ")" << std::endl;
    }
};*/

template <typename T>
using eMap = map<int, T>;

template <typename T>
using vMap = map<int, eMap<T> *>;

template <typename T>
using Vertex = std::pair<int, eMap<T> *>;

template <typename T>
using Edge = std::pair<int,T>;

template <typename T>
class Graph {
    
    private:
    //    vector<helperEdge<T> > edgeList;
    public:
        vMap<T> vertexList_;
        
        Graph(int V);
        Graph(const Graph &obj);
        Graph();
        ~Graph();

        //returns number of vertices of this graph
        int numVertices() const;
        //returns pointer to the begin of the eMap of the num'th Vertex
        typename eMap<T>::const_iterator vertexMapBegin(int num) const;
        //returns pointer to the end of the eMap of the num'th Vertex
        typename eMap<T>::const_iterator vertexMapEnd(int num) const;
        //returns copy of eMap of num'th Vertex
        eMap<T> vertexMap(int num);
        //returns value of edge from VertexOne to VertexTwo if edge exists, reutrns 0 else
        T valEdge(int VertexOne, int VertexTwo);
        //add edge from VertexOne to VertexTwo with weight weight to the graph
        void addEdge(int VertexOne, int VertexTwo, T weight);
        //adds single edge
        void addSingleEdge(int VertexOne, int VertexTwo, T weight);
        //remove edge from VertexOne to VertexTwofrom the graph
        void removeEdge(int VertexOne, int VertexTwo);
        //returns true if a vertex number num exists else returns false
        bool vertexExists(const int num);
        //returns true if a vertex number obj.num exists else returns false
        bool vertexExists(const pair<int,eMap<T> *>);
        //add Vertex obj to graph if no Vertex number num does not exist yet
        void addVertex(const pair<int,eMap<T> *> obj);
        //add Vertex number num to Graph if no vertex number num exists yet
        void addVertex(int num);
        //removes Vertex number num if it exists
        void removeVertex(int num);
        //combine this graph with graph obj to one graph
        void combineWithGraph(Graph obj);
        //prints Graph
        void printGraph() const;
        //print number of vertieces
        void printVertices() const;
        //prints all Edges of Graph
        void printEdges() const;
        /*returns const iterator to begin of edge list
        typename std::vector<helperEdge<T> >::const_iterator EdgeListStart() const;
        //returns const iterator to end of edge list
        typename std::vector<helperEdge<T> >::const_iterator EdgeListEnd() const;
        //returns size of EdgeList
        int EdgeListSize() const;*/
        // empties Graph
        void clear();
};

template <typename T>
Graph<T>::Graph(int V)
{
    for (int i = 0; i < V; ++i)
    {
        eMap<T> *newEdgeList = new eMap<T>;
        this->vertexList_.emplace(i, newEdgeList);
    }
}

template <typename T>
Graph<T>::Graph(const Graph<T> &obj)
{
    for (typename vMap<T>::const_iterator itV = obj.vertexList_.begin(); itV != obj.vertexList_.end(); ++itV)
    {
        eMap<T> *newEdgeList = new eMap<T>;
        *newEdgeList = *itV->second;
        
        this->vertexList_.emplace(itV->first, newEdgeList);
    }
}

template <typename T>
Graph<T>::Graph() {}

template <typename T>
Graph<T>::~Graph()
{
    for (typename vMap<T>::iterator it = this->vertexList_.begin(); it != this->vertexList_.end(); ++it)
    {
        it->second->clear();
        delete it->second;
    }
}

template <typename T>
int Graph<T>::numVertices() const
{
    return this->vertexList_.size();
}

template <typename T>
typename eMap<T>::const_iterator Graph<T>::vertexMapBegin(int num) const
{
    typename vMap<T>::const_iterator it = this->vertexList_.find(num);
    assert(it != this->vertexList_.end());

    return it->second->begin();
}

template <typename T>
typename eMap<T>::const_iterator Graph<T>::vertexMapEnd(int num) const
{
    typename vMap<T>::const_iterator it = this->vertexList_.find(num);
    assert(it != this->vertexList_.end());
    
    return it->second->end();
}

template <typename T>
eMap<T> Graph<T>::vertexMap(int num)
{
    typename vMap<T>::const_iterator it = this->vertexList_.find(num);
    assert(it != this->vertexList_.end());

    return *it->second;
}

template <typename T>
T Graph<T>::valEdge(int VertexOne, int VertexTwo)
{
    typename vMap<T>::const_iterator itV = this->vertexList_.find(VertexOne);
    if(itV == this->vertexList_.end()) {return 0; }

    typename eMap<T>::const_iterator itE = itV->second->find(VertexTwo);
    if(itE == itV->second->end()) {return 0; }
    
    return itE->second;
}

template <typename T>
void Graph<T>::addEdge(int VertexOne, int VertexTwo, T weight)
{
    // add edge to map
    typename vMap<T>::const_iterator itV;
    
    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    itV->second->emplace(VertexTwo,weight);

    itV = this->vertexList_.find(VertexTwo);
    assert(itV != this->vertexList_.end());

    itV->second->emplace(VertexOne,weight);

    /* add edge to edge list
    helperEdge<T> add;
    if (VertexOne <= VertexTwo)
    {
        add.src = VertexOne;
        add.dest = VertexTwo;
        add.weight = weight;
    }
    else
    {
        add.src = VertexTwo;
        add.dest = VertexOne;
        add.weight = weight;
    }
    
    edgeList.push_back(add);*/
}

template <typename T>
void Graph<T>::addSingleEdge(int VertexOne, int VertexTwo, T weight)
{
    typename vMap<T>::const_iterator itV;
            
    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    itV->second->emplace(VertexTwo,weight);
}

template <typename T>
void Graph<T>::removeEdge(int VertexOne, int VertexTwo)
{
    typename vMap<T>::const_iterator itV;
    
    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    itV->second->erase(VertexTwo);

    itV = this->vertexList_.find(VertexTwo);
    assert(itV != this->vertexList_.end());

    itV->second->erase(VertexOne);

    /*remove edge from edgelist
    bool found = false;
    std::vector<helperEdge<T> >::iterator rem;
    for(std::vector<helperEdge<T> >::const_iterator it = this->edgeList.begin(); it != this->edgeList.end() && found != true; ++it)
    {
        if(it->src == VertexOne)
        {
            if(it->des == it->src == VertexTwo)
            {
                rem = it;
                found = true;
            }
        }
        else if(it->src == VertexTwo)
        {
            if(it->des == VertexOne)
            {
                rem = it;
                found = true;
            }
        }
    }

    this->edgeList.emplach(rem);*/
}

template <typename T>
bool Graph<T>::vertexExists(const int num)
{
    typename vMap<T>::const_iterator itV = this->vertexList_.find(num);
    return itV != this->vertexList_.end();
}

template <typename T>
bool Graph<T>::vertexExists(const pair<int,eMap<T> *> obj)
{
    typename vMap<T>::const_iterator itV = this->vertexList_.find(obj.first);
    return itV != this->vertexList_.end();
}

template <typename T>
void Graph<T>::addVertex(const pair<int,eMap<T> *> obj)
{
    eMap<T> *newEdgeList = new eMap<T>;
        
    this->vertexList_.emplace(obj.first, newEdgeList);
}

template <typename T>
void Graph<T>::addVertex(int num)
{
    eMap<T> *newEdgeList = new eMap<T>;
        
    this->vertexList_.emplace(num, newEdgeList);
}

template <typename T>
void Graph<T>::removeVertex(int num)
{
    typename vMap<T>::iterator itV = this->vertexList_.find(num);
    assert(itV != this->vertexList_.end());

    // remove edges form lists of other Vertices
    for (typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE)
    {
        typename vMap<T>::iterator itVRem = this->vertexList_.find(itE->first);
        assert(itVRem != this->vertexList_.end());

        itVRem->second->erase(num);
    }

    itV->second->clear();
    delete itV->second;

    this->vertexList_.erase(itV);
}

template <typename T>
void Graph<T>::combineWithGraph(Graph<T> obj)
{

}

template <typename T>
void Graph<T>::printGraph() const
{
    cout << endl;
    for(typename vMap<T>::const_iterator itV = this->vertexList_.begin(); itV != this->vertexList_.end(); ++itV)
    {
        cout << itV->first << ":" << endl;
        for(typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE)
        {
            cout << "Edge to " << itE->first << " with weight " << itE->second << endl;
        }
    }
}

template <typename T>
void Graph<T>::printVertices() const
{
    cout << endl << "Number of vertices: ";

    for(typename vMap<T>::const_iterator itV = this->vertexList_.begin(); itV != this->vertexList_.end(); ++itV)
    {
        cout << itV->first << ", " ;
    }
    cout << endl;
}

template <typename T>
void Graph<T>::printEdges() const
{
    cout << endl << "List of Edges:" << endl;
    
    for(typename vMap<T>::const_iterator itV = this->vertexList_.begin(); itV != this->vertexList_.end(); ++itV)
    {
        for(typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE)
        {
            if(itE->first > itV->first)
            {
                cout << "(" << itV->first << "," << itE->first << "," << itE->second << ")" << endl;
            }
        }
    }
}

/*
template <typename T>
typename std::vector<helperEdge<T> >::const_iterator Graph<T>::EdgeListStart() const
{
    return this->edgeList.begin();
}

template <typename T>
typename std::vector<helperEdge<T> >::const_iterator Graph<T>::EdgeListEnd() const
{
    return this->edgeList.end();
}

template <typename T>
int Graph<T>::EdgeListSize() const
{
    return this->edgeList.size();
}
*/

template <typename T>
void Graph<T>::clear()
{
    for (typename vMap<T>::iterator it = this->vertexList_.begin(); it != this->vertexList_.end(); ++it)
    {
        it->second->clear();
        delete it->second;
    }
    vertexList_.clear();
}

#endif  /* CLASSMAPGRAPH_H */