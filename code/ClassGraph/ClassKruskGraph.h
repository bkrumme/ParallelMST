#ifndef CLASSKRUSKGRAPH_H
#define CLASSKRUSKGRAPH_H

#include <iostream>
#include <map>
#include <set>
#include <assert.h>
#include <algorithm>

using namespace std;

template <typename T>
using eMap = std::multimap<T,int>;

template <typename T>
using vMap = std::map<int, eMap<T> *>;

template <typename T>
using Vertex = std::pair<int, eMap<T> *>;

template <typename T>
using Edge = std::pair<T,int>;

template <typename T>
class Graph {
    
    private:
        vMap<T> vertexList_;
    public:
        
        Graph(int V);
        Graph(const Graph &obj);
        Graph();
        ~Graph();

        //returns number of vertices of this graph
        int numVertices() const;
        //returns pointer to the begin of the eMap of the num'th Vertex
        typename eMap<T>::const_iterator vertexMapBegin(int num) const;
        //returns pointer to the end of the eMap of the num'th Vertex
        typename eMap<T>::const_iterator vertexMapEnd(int num) const;
        //returns value of edge from VertexOne to VertexTwo if edge exists, reutrns 0 else
        T valEdge(int VertexOne, int VertexTwo);
        //add edge from VertexOne to VertexTwo with weight weight to the graph
        void addEdge(int VertexOne, int VertexTwo, T weight);
        //adds single edge
        void addSingleEdge(int VertexOne, int VertexTwo, T weight);
        //remove edge from Vertex One to VertexTwo from the graph
        void removeEdge(int VertexOne, int VertexTwo);
        //remove edge from Vertex One to VertexTwo from the graph
        void removeSingleEdge(int VertexOne, int VertexTwo);
        //returns true if a vertex number num exists else returns false
        bool vertexExists(const int num);
        //returns true if a vertex number obj.num exists else returns false
        bool vertexExists(const pair<int,eMap<T> *>);
        //add Vertex obj to graph if no Vertex number num does not exist yet
        void addVertex(const pair<int,eMap<T> *> obj);
        //add Vertex number num to Graph if no vertex number num exists yet
        void addVertex(int num);
        //removes Vertex number num if it exists
        void removeVertex(int num);
        //prints Graph
        void printGraph() const;
        //print number of vertieces
        void printVertices() const;
        //prints all Edges of Graph
        void printEdges() const;
        //clears Graph
        void clear();
};

template <typename T>
Graph<T>::Graph(int V)
{
    for (int i = 0; i < V; ++i)
    {
        eMap<T> *newEdgeList = new eMap<T>;
        this->vertexList_.emplace(i, newEdgeList);
    }
}

template <typename T>
Graph<T>::Graph(const Graph<T> &obj)
{
    for (typename vMap<T>::const_iterator itV = obj.vertexList_.begin(); itV != obj.vertexList_.end(); ++itV)
    {
        eMap<T> *newEdgeList = new eMap<T>;
        *newEdgeList = *itV->second;
        
        this->vertexList_.emplace(itV->first, newEdgeList);
    }
}

template <typename T>
Graph<T>::Graph() {}

template <typename T>
Graph<T>::~Graph()
{
    for (typename vMap<T>::iterator it = this->vertexList_.begin(); it != this->vertexList_.end(); ++it)
    {
        it->second->clear();
        delete it->second;
    }
}

template <typename T>
int Graph<T>::numVertices() const
{
    return this->vertexList_.size();
}

template <typename T>
typename eMap<T>::const_iterator Graph<T>::vertexMapBegin(int num) const
{
    typename vMap<T>::const_iterator it = this->vertexList_.find(num);
    assert(it != this->vertexList_.end());

    return it->second->begin();
}

template <typename T>
typename eMap<T>::const_iterator Graph<T>::vertexMapEnd(int num) const
{
    typename vMap<T>::const_iterator it = this->vertexList_.find(num);
    assert(it != this->vertexList_.end());
    
    return it->second->end();
}

template <typename T>
T Graph<T>::valEdge(int VertexOne, int VertexTwo)
{
    typename vMap<T>::const_iterator itV = this->vertexList_.find(VertexOne);
    if(itV == this->vertexList_.end()) {return 0; }
    
    for(typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE){
        if(itE->second == VertexTwo) { return itE->first; }
    }

    return 0;
}


template <typename T>
void Graph<T>::addEdge(int VertexOne, int VertexTwo, T weight)
{
    // add edge to map
    typename vMap<T>::const_iterator itV;
    
    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    itV->second->emplace(weight, VertexTwo);

    itV = this->vertexList_.find(VertexTwo);
    assert(itV != this->vertexList_.end());

    itV->second->emplace(weight,VertexOne);
}

template <typename T>
void Graph<T>::addSingleEdge(int VertexOne, int VertexTwo, T weight)
{
    typename vMap<T>::const_iterator itV;

    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    itV->second->emplace(weight, VertexTwo);
}

template <typename T>
void Graph<T>::removeEdge(int VertexOne, int VertexTwo)
{
    typename vMap<T>::iterator itV;
    
    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    bool erased = false;
    for(typename eMap<T>::iterator itE = itV->second->begin(); itE != itV->second->end() && erased==false; ++itE){
        if(itE->second == VertexTwo)
        { 
            itV->second->erase(itE);
            erased = true;
        }
    }

    itV = this->vertexList_.find(VertexTwo);
    assert(itV != this->vertexList_.end());

    erased = false;
    for(typename eMap<T>::iterator itE = itV->second->begin(); itE != itV->second->end() && erased==false; ++itE){
        if(itE->second == VertexOne)
        { 
            itV->second->erase(itE);
            erased = true;
        }
    }
}

template <typename T>
void Graph<T>::removeSingleEdge(int VertexOne, int VertexTwo)
{
    typename vMap<T>::iterator itV;
    
    itV = this->vertexList_.find(VertexOne);
    assert(itV != this->vertexList_.end());

    bool erased = false;
    for(typename eMap<T>::iterator itE = itV->second->begin(); itE != itV->second->end() && erased==false; ++itE){
        if(itE->second == VertexTwo)
        { 
            itV->second->erase(itE);
            erased = true;
        }
    }
}

template <typename T>
bool Graph<T>::vertexExists(const int num)
{
    typename vMap<T>::const_iterator itV = this->vertexList_.find(num);
    return itV != this->vertexList_.end();
}

template <typename T>
bool Graph<T>::vertexExists(const pair<int,eMap<T> *> obj)
{
    typename vMap<T>::const_iterator itV = this->vertexList_.find(obj.first);
    return itV != this->vertexList_.end();
}

template <typename T>
void Graph<T>::addVertex(const pair<int,eMap<T> *> obj)
{
    eMap<T> *newEdgeList = new eMap<T>;
        
    this->vertexList_.emplace(obj.first, newEdgeList);
}

template <typename T>
void Graph<T>::addVertex(int num)
{
    eMap<T> *newEdgeList = new eMap<T>;
        
    this->vertexList_.emplace(num, newEdgeList);
}

template <typename T>
void Graph<T>::removeVertex(int num)
{
    typename vMap<T>::iterator itV = this->vertexList_.find(num);
    assert(itV != this->vertexList_.end());

    // remove edges form lists of other Vertices
    bool erased;
    for (typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE)
    {
        typename vMap<T>::iterator itVRem = this->vertexList_.find(itE->second);
        assert(itVRem != this->vertexList_.end());

        erased = false;
        for(typename eMap<T>::iterator itERem = itVRem->second->begin(); itERem != itVRem->second->end() && erased==false; ++itERem){
            if(itERem->second == num)
            { 
                itVRem->second->erase(itERem);
                erased = true;
            }
        }
    }

    itV->second->clear();
    delete itV->second;

    this->vertexList_.erase(itV);
}

template <typename T>
void Graph<T>::printGraph() const
{
    cout << endl;
    for(typename vMap<T>::const_iterator itV = this->vertexList_.begin(); itV != this->vertexList_.end(); ++itV)
    {
        cout << itV->first << ":" << endl;
        for(typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE)
        {
            cout << "Edge to " << itE->second << " with weight " << itE->first << endl;
        }
    }
}

template <typename T>
void Graph<T>::printVertices() const
{
    cout << endl << "Number of vertices: ";

    for(typename vMap<T>::const_iterator itV = this->vertexList_.begin(); itV != this->vertexList_.end(); ++itV)
    {
        cout << itV->first << ", " ;
    }
    cout << endl;
}

template <typename T>
void Graph<T>::printEdges() const
{
    cout << endl << "List of Edges:" << endl;
    
    for(typename vMap<T>::const_iterator itV = this->vertexList_.begin(); itV != this->vertexList_.end(); ++itV)
    {
        for(typename eMap<T>::const_iterator itE = itV->second->begin(); itE != itV->second->end(); ++itE)
        {
            if(itE->first > itV->first)
            {
                cout << "(" << itV->first << "," << itE->second << "," << itE->first << ")" << endl;
            }
        }
    }
}

template <typename T>
void Graph<T>::clear()
{
    for (typename vMap<T>::iterator it = this->vertexList_.begin(); it != this->vertexList_.end(); ++it)
    {
        it->second->clear();
        delete it->second;
    }
    vertexList_.clear();
}

#endif  /* CLASSKRUSKGRAPH_H */
