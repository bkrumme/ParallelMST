#include <iostream>
#include <vector>
#include "ClassKruskGraph.h"

using namespace std;

int main ()
{
    Graph<int> FirstTry; //empty Graph constructor
    cout << "graph created" << endl;
    FirstTry.addVertex(1);  //function addVertex
    FirstTry.addVertex(2);
    FirstTry.addVertex(4);
    FirstTry.addVertex(3);
    FirstTry.addVertex(20);
    FirstTry.addVertex(13);

    cout << "Vertex added" << endl;

    FirstTry.addEdge(1,2,4);    //function addEdge
    FirstTry.addEdge(1,4,2);
    FirstTry.addEdge(3,20,5);
    FirstTry.addEdge(1,13,2);
    cout << "Num Vertices " << FirstTry.numVertices() << endl;

    bool ex = FirstTry.valEdge(1,2);
    cout << endl << "Edge between 1 and 2 exists: edgeExists(1,2) = " << ex << endl;

    cout << endl << "Value of edge between 1 and 2 is 4: valEdge(1,2) = " << FirstTry.valEdge(1,2) << endl;

    FirstTry.removeEdge(1,2);
    ex = FirstTry.valEdge(1,2);
    cout << endl << "Edge between 1 and 2 does not exists anymore after removal: edgeExists(1,2) = " << ex << endl;

    Graph<double> Secound(5);   //Graph with 5 verteces
    Secound.addEdge(1,3,6.5);

    cout << endl << "Vertex number 1 exists in graph Secound: vertexExists(1) = " << Secound.vertexExists(1) << endl;
    
    pair<int,eMap<double> *> test;
    test.first = 1;
    cout << endl << "Vertex test with number 1 exists in graph Secound: vertexExists(test) = " << Secound.vertexExists(test) << endl;

    Secound.addVertex(10);
    cout << endl << "After adding vertex number 10 to graph Secound vertex num 10 exists = " << Secound.vertexExists(10) << endl;

    pair<int,eMap<double> *> add;
    add.first = 11;
    Secound.addVertex(add);
    cout << endl << "After adding a vertex with the number 11 to graph Secound a vertex with num 11 exists = " << Secound.vertexExists(add) << endl;

    Secound.addEdge(1,10,3.3);

    Secound.removeVertex(10);
    cout <<  endl << "After removing vertex number 10 from graph Secound vertex num 10 exists = " << Secound.vertexExists(10) << endl;
    FirstTry.removeVertex(13);  
    FirstTry.printGraph();
    FirstTry.printVertices();
    FirstTry.printEdges();
    Secound.printGraph();
    Secound.printVertices();
    Secound.printEdges();

    return 0;
}