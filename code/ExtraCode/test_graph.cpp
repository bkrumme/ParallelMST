#include <iostream>
#include "ClassGraph.h"

int main(){

	Graph g(10);
	g.addEdge(6,5,1);
	g.addEdge(5,2,2);
	g.addEdge(8,3,3);
	g.addEdge(6,1,4);
	g.addEdge(3,1,5);
	g.printGraph();


	return 0;
}