#include <iostream>
#include <list>
#include <vector>
#include <assert.h>
#include <algorithm>
#include <set>

using namespace std;

struct Edge
{
    int dest;
    int weight;

    //constructor for use with set find
    Edge(const int& input_dest = -1, const int& input_weight = -1000)
    : dest(input_dest), weight(input_weight) {}

    // only to be used an Edges from the same source
    bool operator==(const struct Edge& rhs)
    {
        return rhs.dest == this->dest && rhs.weight == this->weight;
    }
};

struct Vertex{
    int num;
    set<struct Edge, struct comp>* adjPtr;
    bool active;
};

struct comp{
    bool operator() (const Edge& lhs, const Edge& rhs) const{
        return lhs.weight < rhs.weight;
    }
};

class Graph {
    
    private:
        vector<struct Vertex> vec_;
        //returns iterator to edge
        inline set<struct Edge, struct comp>::iterator edgeIterator(const int VertexOne, const int VertexTwo);
        //returns const_iterator to edge
        inline set<struct Edge, struct comp>::const_iterator edgeIterator(const int VertexOne, const int VertexTwo) const;

    public:
        Graph(int V);
        Graph(const Graph &obj);
        Graph();
        ~Graph();

        //returns number of vertices of this graph
        int numVertices() const;

        //returns true if an edge from VertexOne to VertexTwo exists in the graph else false
        bool edgeExists(const int VertexOne, const int VertexTwo) const;

        //returns value of edge from VertexOne to VertexTwo
        int valEdge(const int VertexOne, const int VertexTwo) const;

        //add edge from VertexOne to VertexTwo with weight weight to the graph
        void addEdge(const int VertexOne, const int VertexTwo, const int weight);

        //remove edge from VertexOne to VertexTwofrom the graph
        void removeEdge(const int VertexOne, const int VertexTwo);

        //returns true if a vertex number num exists else returns false
        bool vertexExists(const int num) const;

        //returns true if a vertex number obj.num exists else returns false
        bool vertexExists(const struct Vertex obj) const;

        //prints Graph
        void printGraph() const;

        //returns iterator to smallest edge of a vertex i
        set<struct Edge, struct comp>::const_iterator smallestEdge(const int i) const;

        //returns the number of edges of a vertex i
        int numEdges(int i) const;

        //returns pointer to edgevector of vertex i
        set<struct Edge, struct comp>* edgeVectorPointer(int i) const;

        //activate vertex i
        void activateVertex(const int i);
};