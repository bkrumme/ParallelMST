#include "ClassGraphVec.h"

Graph::Graph(int V){       
    vec_.reserve(V);
    for(int i = 0; i < V; i++){
        set<struct Edge, struct comp>* temp_ptr = new set<struct Edge, struct comp>; //evtl add size
        struct Vertex temp_vertex{i, temp_ptr, false};
        vec_.push_back(temp_vertex);
    }
}

Graph::Graph(const Graph &obj) : vec_(vector<struct Vertex>()){
    vec_.reserve(obj.numVertices());
    for (int i=0; i<obj.numVertices(); ++i)
    {
        set<struct Edge, struct comp>* temp_ptr = new set<struct Edge, struct comp>; //evtl add size
        *temp_ptr = *(obj.edgeVectorPointer(i));
        bool temp_active = obj.vec_.at(i).active;
        struct Vertex temp_vertex{i, temp_ptr, temp_active};
        vec_.push_back(temp_vertex);
    }       



}
Graph::Graph() : vec_(vector<struct Vertex>()) {}

Graph::~Graph(){
    for (vector<struct Vertex>::iterator it = this->vec_.begin(); it != this->vec_.end(); ++it){
       delete it->adjPtr;
    }
}

inline set<struct Edge, struct comp>::iterator Graph::edgeIterator(const int VertexOne, const int VertexTwo){
       return this->vec_.at(VertexOne).adjPtr->find(VertexTwo);
}
inline set<struct Edge, struct comp>::const_iterator Graph::edgeIterator(const int VertexOne, const int VertexTwo) const{
    return this->vec_.at(VertexOne).adjPtr->find(VertexTwo);
}                               


int Graph::numVertices() const {
    return this->vec_.size();
}

bool Graph::edgeExists(const int VertexOne, const int VertexTwo) const{
    assert( this->vertexExists(VertexOne) && "VertexOne doesn't exist in graph");
    assert( this->vertexExists(VertexTwo) && "VertexTwo doesn't exist in graph");

    set<struct Edge, struct comp>::const_iterator it1 = this->edgeIterator(VertexOne,VertexTwo);
    if(it1 == this->vec_.at(VertexOne).adjPtr->end()) return false;
    else return true;
}

int Graph::valEdge(const int VertexOne, const int VertexTwo) const{
    assert(this->edgeExists(VertexOne,VertexTwo) && "Edge existiert nicht");
    return this->edgeIterator(VertexOne,VertexTwo)->weight;
    
}

void Graph::addEdge(const int VertexOne, const int VertexTwo, const int weight){
    assert(!this->edgeExists(VertexOne,VertexTwo) && "Edge exisitiert bereits");
    assert(VertexOne != VertexTwo && "addEdge would create a self-loop");
    assert( this->vertexExists(VertexOne) && "VertexOne doesn't exist in graph");
    assert( this->vertexExists(VertexTwo) && "VertexTwo doesn't exist in graph");
    
    struct Edge EdgeOne;
    EdgeOne.dest = VertexTwo;
    EdgeOne.weight = weight;
    this->vec_.at(VertexOne).adjPtr->emplace(EdgeOne);

    struct Edge EdgeTwo;
    EdgeTwo.dest = VertexOne;
    EdgeTwo.weight = weight;
    this->vec_.at(VertexTwo).adjPtr->emplace(EdgeTwo);
}

void Graph::removeEdge(const int VertexOne, const int VertexTwo){
    assert(this->edgeExists(VertexOne,VertexTwo) && "Edge that should be removed doesn't exist");
    assert(VertexOne >= 0 && VertexOne < this->numVertices() && "VertexOne doesn't exist in graph");
    assert(VertexTwo >= 0 && VertexTwo < this->numVertices() && "VertexTwo doesn't exist in graph");
    
    set<struct Edge, struct comp>* adjPtrOne = this->vec_.at(VertexOne).adjPtr;
    set<struct Edge, struct comp>::iterator it1 = adjPtrOne->find(VertexTwo);
    if(it1 == adjPtrOne->end()) assert("VertexOne nicht gefunden");
    adjPtrOne->erase(it1);

    set<struct Edge, struct comp>* adjPtrTwo = this->vec_.at(VertexTwo).adjPtr;
    set<struct Edge, struct comp>::iterator it2 = adjPtrTwo->find(VertexOne);
    if(it2 == adjPtrTwo->end()) assert("VertexTwo nicht gefunden");
    adjPtrTwo->erase(it2);
}


bool Graph::vertexExists(const int num) const{
    return num >= 0 && num < this->numVertices();
}

bool Graph::vertexExists(const struct Vertex obj) const{
    return this->vertexExists(obj.num);
}

void Graph::printGraph() const {
    for (vector<struct Vertex>::const_iterator it = this->vec_.begin(); it != this->vec_.end(); it++){
        for(set<struct Edge, struct comp>::const_iterator itEdge = it->adjPtr->begin(); itEdge != it->adjPtr->end(); ++itEdge){
            cout << "( " << it->num << ", " <<  itEdge->dest << " ) weight: "<< itEdge->weight << endl;
        }
    }
}

set<struct Edge, struct comp>::const_iterator Graph::smallestEdge(const int i) const{
    assert(vertexExists(i) && "vertex doesn't exist");

    //std::set is ordered -> just return iterator to first element
    set<struct Edge, struct comp>::const_iterator min_edge_it = this->vec_.at(i).adjPtr->begin();
    return min_edge_it;


}

int Graph::numEdges(int i) const {
    return (vec_.at(i).adjPtr)->size();
} 

set<struct Edge, struct comp>* Graph::edgeVectorPointer(int i) const{
    return vec_.at(i).adjPtr;
}

void Graph::activateVertex(const int i){
    this->vec_.at(i).active = true;
}