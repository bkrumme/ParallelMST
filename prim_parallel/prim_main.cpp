#include <mpi.h>
#include <set>
#include "../matrix_input/mmio.h"
#include <bits/stdc++.h>
#include <tuple>
#include <unistd.h>
#include <fstream>

// uncomment next line to use with doubles
//#define use_integers

#ifdef use_integers
#define mat_t int
#define mpi_mat_t MPI_INT
#define readin(rank, size, argc, argv) mtx_to_subgraph(rank, size, argc, argv)
#define MAX_VALUE INT_MAX
#else
#define mat_t double  
#define mpi_mat_t MPI_DOUBLE
#define readin(rank, size, argc, argv) mtx_to_subgraph_double(rank, size, argc, argv)
#define MAX_VALUE DBL_MAX
#endif




typedef struct {
	mat_t w; //weight
	int v;   //vertex
	int s;   //src
} edge_struct;

struct edgeComp{
	bool operator()( const edge_struct& lhs, const edge_struct& rhs) {
		if(lhs.w == rhs.w){
			if(lhs.v == rhs.v){
				return lhs.s < rhs.s;
			}
			else return lhs.v < rhs.v;
		}
		else return lhs.w < rhs.w;
	}
};

bool operator==(const edge_struct& lhs, const edge_struct& rhs){
	return lhs.w == rhs.w && lhs.v == rhs.v && lhs.s == rhs.s;
}

void smallestEdgeFunction(void *in, void *inout, int *len, MPI_Datatype *dptr){
	edge_struct *a = (edge_struct*) in;
	edge_struct *b = (edge_struct*) inout;

	edge_struct temp;
	for(int i = 0; i < *len; i++){
		if(a->w == b->w){
			temp.w = a->w;
			if(a->v == b->v){
				temp.v = a->v;
				temp.s = std::min(a->s, b->s);
			}
			else{
				if(a->v < b->v){
					temp.v = a->v;
					temp.s = a->s;
				}
				else{
					temp.v = b->v;
					temp.s = b->s;
				}				
			}
		}
		else{
			if(a->w < b->w){
				temp = *a;
			}
			else{
				temp = *b;
			}
		}
		*b = temp;
		a++; b++; 
	}
}

int main(int argc, char *argv[]){
    MPI_Init(&argc, &argv);
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    double time = 0;
    if(rank == 0){
    	time = -MPI_Wtime();
    }

    // make new MPI datatype, to reduce number of communication steps
    MPI_Datatype MPI_edge;
	const int block_length[3] = {1, 1, 1};
	const MPI_Aint displacement[3] = {offsetof(edge_struct, w), offsetof(edge_struct, v), offsetof(edge_struct, s)};
	const MPI_Datatype types[3] = {mpi_mat_t, MPI_INT, MPI_INT};
	MPI_Type_create_struct(3, block_length, displacement, types, &MPI_edge);
	MPI_Type_commit(&MPI_edge);

	// make new MPI operator to use on MPI_edge
	MPI_Op smallestEdge;
	MPI_Op_create(smallestEdgeFunction, true, &smallestEdge);


	const mat_t max_val = MAX_VALUE;

	//return graph in adjacency list
    Graph<mat_t> graph = readin(rank, size, argc, argv);

    //total Number of Vertices 
	const int numVertices = mtx_numVertices(argc, argv);

	// any Start_Vertex < numVertices possible
	int Start_Vertex = 0;

	edge_struct local_min;
	edge_struct global_min;

	edge_struct max_template;
	max_template.w = max_val;
	max_template.s = INT_MAX;

	// create Cost vector, contains minimal cost from each Vertex of this processor to the MST
	// dont include start_vertex
	multiset<edge_struct,edgeComp> Cost;
	for(int i = rank; i < numVertices; i+=size){
		max_template.v = i;
		if(i!=Start_Vertex){
			Cost.emplace(max_template);
		}
	}
	max_template.v = INT_MAX;

	int lastAddedVertex = Start_Vertex;

	//empty multiset, stores elements that are changed and need reordering
	multiset<edge_struct,edgeComp> reorder; 

if(rank == 0){	
	// Process 0 stores all the MST information
	Graph<mat_t> MST;
	MST.addVertex(Start_Vertex);
	// if more than one tree, store them and their sum in vector trees
	vector<pair<Graph<mat_t>,mat_t>> trees;
	// sum of weights of edges, to check correctness
	mat_t sum = 0;


	for(int i = 1; i < numVertices; i++){
		// Cost Vector of this Process is empty -> always input Max
		if(Cost.empty()){
			MPI_Allreduce(&max_template, &global_min, 1, MPI_edge, smallestEdge, MPI_COMM_WORLD);
			//go into for loop here, spin and the break in the end!	
		}

		else{
			// ordering of Cost Vector (pack in function)
			// iteration of j gets handeled in the loop, because when tuple is erased, pointer gets lost...
			for(multiset<edge_struct>::iterator j = Cost.begin(); j != Cost.end(); ){

				// Edge von Vertex zu MST bzw zum letzen neu hinzugefügen Vertex exisitert und ist kleiner als bisherige Edge
				mat_t valEdge = graph.valEdge(j->v, lastAddedVertex);
				if(valEdge != 0 && valEdge < j->w){
					edge_struct temp = {valEdge, j->v, lastAddedVertex};
					reorder.insert(temp);
					// erase j and advance to next tuple
					j = Cost.erase(j);
				}
				else j++;

			}
			//Cost.merge(reorder); // Euler compilers don't yet supprot merge (c++17)
			for(multiset<edge_struct>::iterator i = reorder.begin(); i != reorder.end(); i++){
				Cost.insert(*i);
			}
			reorder.clear();

			local_min = *Cost.begin();
			MPI_Allreduce(&local_min, &global_min, 1, MPI_edge, smallestEdge, MPI_COMM_WORLD);
	
		}

		if(global_min == local_min){
			Cost.erase(Cost.begin());
		}

		
		// when Vertex belongs to a new tree
		if(global_min.w == max_val){
			//store MST in trees and clear MST
			trees.push_back(make_pair(MST,sum));
			MST.clear();
			sum = 0.0;
			// Start_Vertex is min (b.c. it's easy to delete)
			Start_Vertex = global_min.v;
			MST.addVertex(Start_Vertex);
			lastAddedVertex = Start_Vertex; 
		}
		else{
			MST.addVertex(global_min.v);
			MST.addEdge(global_min.s, global_min.v, global_min.w);
			sum += global_min.w;
			lastAddedVertex = global_min.v;						
		}
	}

	trees.push_back(make_pair(MST,sum));
	mat_t total_sum = 0;
	for(size_t i = 0; i < trees.size(); i++){
		//cout << "MST "<<i<<"  sum: "<< trees[i].second;
		//cout << "  numVertices  "<<trees.at(i).first.numVertices()  << endl;
		total_sum += trees[i].second;
	}

	cout << "number of MSTs: " << trees.size() << endl;
	cout << "total sum of all MSTs: " << total_sum << endl;
	//trees[0].first.printGraph();

	time += MPI_Wtime();
    cout << "Time: "<< time << endl;
    ofstream timefile;
    timefile.open("times.txt", ios::app);
    timefile << size<< " "<<time << endl;
    timefile.close();

} 
else{ // rank != 0

	for(int i = 1; i < numVertices; i++){ 


		// Cost Vector of this Process is empty -> always imput Max
		if(Cost.empty()){
			MPI_Allreduce(&max_template, &global_min, 1, MPI_edge, smallestEdge, MPI_COMM_WORLD);
			//go into for loop here, spin and the break in the end!			
		}
		else{
			for(multiset<edge_struct>::iterator j = Cost.begin(); j != Cost.end(); ){

				// Edge von Vertex zu MST bzw zum letzen neu hinzugefügen Vertex exisitert
				mat_t valEdge = graph.valEdge(j->v, lastAddedVertex);
				if(valEdge != 0 && valEdge < j->w){
					edge_struct temp = {valEdge, j->v, lastAddedVertex};
					reorder.insert(temp);
					// erase j and advance to next tuple
					j = Cost.erase(j);
				}
				else j++;				
			}
			//Cost.merge(reorder); // Euler compilers don't yet supprot merge (c++17)
			for(multiset<edge_struct>::iterator i = reorder.begin(); i != reorder.end(); i++){
				Cost.insert(*i);
			}
			reorder.clear();
			
			local_min = *Cost.begin();

			MPI_Allreduce(&local_min, &global_min, 1, MPI_edge, smallestEdge, MPI_COMM_WORLD);
			if(global_min == local_min){
				Cost.erase(Cost.begin());
			}
			lastAddedVertex = global_min.v;		
			
		}	
		
	}
}
  
	MPI_Finalize();
	return 0;	
}