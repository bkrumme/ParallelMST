#include "../../matrix_input/mmio.h"
#include <stdio.h>
#include <bits/stdc++.h>
#include <unistd.h>

#define mat_t int

struct subset{
    int parent;
    int rank;
};

//template<typename T>
struct helper_edge{
    int src;
    int dest;
    double weight;

    void helper_print(){
        std::cout << "(" << src << "," << dest << "," << weight << ")" << std::endl;
    }
};


// find the set of element i
int find(struct subset subsets[], int i){
    if(subsets[i].parent != i){
        subsets[i].parent = find(subsets, subsets[i].parent);
    }

    return subsets[i].parent;
}

// a function to union two sets
void Union(struct subset subsets[], int x, int y){
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);

    // Attach smaller rank tree under root of high 
    if(subsets[xroot].rank < subsets[yroot].rank){
        subsets[xroot].parent = yroot; 
    }
    else if(subsets[xroot].rank > subsets[yroot].rank){
        subsets[yroot].parent = xroot;
    }
    // If ranks are same, then make one as root and increment its rank by one
    else {
        subsets[yroot].parent = xroot; 
        ++subsets[xroot].rank;
    }
}

Graph<mat_t> boruvka(Graph<mat_t> graph, int V){
    // initialize the graph and the weight
    int numTrees = V;
    int MSTweight = 0;
    int numEdges = 0;
    Graph<mat_t> MST(V);

    // for(int i = 0; i < V; ++i){
    //     MST.addVertex(i);
    // }

    // Allocate memory
    struct subset *subsets = new subset[V];

    // An array to store index of the cheapest edge of 
    // subset
    int *cheapest = new int[V];

    // Create V subsets with single elements 
    for (int v = 0; v < V; ++v)
    { 
        subsets[v].parent = v; 
        subsets[v].rank = 0; 
        cheapest[v] = INT_MIN; 
    }


    std::vector<helper_edge> edges;
    helper_edge e; 

    for(int i = 0; i < V; ++i){
        for(int j = i; j < V; ++j){
            if(graph.valEdge(i,j) != 0){
                e.src = i;
                e.dest = j;
                e.weight = graph.valEdge(i,j);
                edges.push_back(e);
                ++numEdges;
            }
        }
    }



    // Loop until there is only one tree left
    while(numTrees > 1){
        
        // Always initialize cheapest array
        for(int v = 0; v < V; ++v){
            cheapest[v] = INT_MIN;
        }

        // Traverse through all edges and update 
        // cheapest of every component
        for (int i=0; i<numEdges; i++){
            // Find components (or sets) of two corners 
            // of current edge
            int set1 = find(subsets, edges[i].src); 
            int set2 = find(subsets, edges[i].dest);
            //std::cout << set1 << std::setw(10) << set2 << std::endl;

            if(set1 == set2)
                continue;
            else{
                if (cheapest[set1] == INT_MIN || edges[cheapest[set1]].weight > edges[i].weight)
                {
                    cheapest[set1] = i;
                }

                if (cheapest[set2] == INT_MIN || edges[cheapest[set2]].weight > edges[i].weight)
                    cheapest[set2] = i;
            }
        }


        // Consider the above picked cheapest edges and add them to MST
        for (int i=0; i<V; i++){
            if (cheapest[i] != INT_MIN){
                int set1 = find(subsets, edges[cheapest[i]].src); 
                int set2 = find(subsets, edges[cheapest[i]].dest);

                if (set1 == set2){
                    continue;
                } 
                
                MSTweight += edges[cheapest[i]].weight;
                MST.addEdge(edges[cheapest[i]].src, edges[cheapest[i]].dest, edges[cheapest[i]].weight);
                printf("Edge %d-%d included in MST\n", edges[cheapest[i]].src, edges[cheapest[i]].dest);

                Union(subsets, set1, set2); 
                numTrees--;
                std::cout << "MSTWeight is : " << MSTweight << std::endl;
            }
        }     

    }

    return MST;
}


int main(int argc, char *argv[]){

    // get graph from matrix
    //Graph<mat_t> graph = mtx_to_graph_double(argc, argv);
    Graph<mat_t> graph = mtx_to_graph(argc, argv);

    // total number of vertices
    const int V = graph.numVertices();


    Graph<mat_t> MST = boruvka(graph, V);

    
    //MST.printGraph();

    return 0;
}
