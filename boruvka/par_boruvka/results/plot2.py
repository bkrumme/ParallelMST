import numpy as np
import matplotlib.pyplot as plt


#data input
data_file = open("bcsstk35_128.txt", 'r')
raw_data = []
raw_data = [line.split() for line in data_file]

for i in raw_data:
	i[0] = int(i[0])
	i[1] = float(i[1])
	#del i[2]


# graph number of cores plotted
max_processors = 128
processor_cores = range(0,max_processors+1,4)
processor_cores[0] = 1
print(processor_cores)


# plot_data[i][0] is the number of processes
# plot_data[i][j] (j = 1 to 5) is the time for the number of processes 
plot_data = []
for i in processor_cores:
	temp = []
	temp.append(i)
	plot_data.append(temp)

for i in raw_data:
	if(i[0] in processor_cores):
		index = processor_cores.index(i[0])
#		print(index)
#		print(i[0])
#		print("---------")
		plot_data[index].append(i[1])


print(plot_data)
print(".------------------.")

num_measurements = 5
# x axis
xdata = np.array(processor_cores)

# compute average
avg = [0.0] * len(processor_cores)
for i in range(len(avg)):
	for j in range(num_measurements):
		avg[i] += plot_data[i][j+1]
	avg[i] /= num_measurements
avgdata = np.array(avg)

# compute minima
mins = []
for i in plot_data:
	mins.append(i[1])
for i in range(len(mins)):
	for j in range(num_measurements-1):
		mins[i] = min(mins[i], plot_data[i][j+2])
mindata = np.array(mins)
# compute maxima
maxs = []
for i in plot_data:
	maxs.append(i[1])
for i in range(len(maxs)):
	for j in range(num_measurements-1):
		maxs[i] = max(maxs[i], plot_data[i][j+2])
maxdata = np.array(maxs)




print(xdata)
print(avgdata)
print(mindata)
print(maxdata)

plt.plot(xdata, avgdata, color='black')
#plt.plot(xdata, mindata)
#plt.plot(xdata, maxdata)
plt.fill_between(xdata, mindata, maxdata, color='gray', alpha=0.2)
plt.show()