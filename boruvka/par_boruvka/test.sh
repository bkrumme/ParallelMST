#!/bin/bash

for i in 1 2 3 4 5
do
	mpirun -np 1 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 4 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 8 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 12 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 16 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 20 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 24 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 28 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 32 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 36 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 40 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 44 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 48 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 52 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 56 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 60 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 64 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 68 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 72 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 76 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 80 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 84 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 88 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 92 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 96 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 100 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 104 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 108 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 112 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 116 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 120 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 124 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 128 ./boruvka ../../matrix_input/bcsstk35/bcsstk35.mtx
done
