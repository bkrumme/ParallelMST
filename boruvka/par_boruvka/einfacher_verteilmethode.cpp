#include <iostream>

int main(){
	int size = 4;
	int numEdges = 17;

	int MinNumEdges = numEdges/size;
	int MaxNumEdges = MinNumEdges + 1;

	int NumProcessorsWithOneExtraEdge = numEdges % size;


	int *sendcounts = new int[size];



	for(int i = 0; i < NumProcessorsWithOneExtraEdge; i++){
		sendcounts[i] = MinNumEdges;
	}
	for(int i = NumProcessorsWithOneExtraEdge; i < size; i++){
		sendcounts[i] = MaxNumEdges;
	}

	for(int i = 0; i < size; i++){
		std::cout << sendcounts[i] << " ";
	}
}