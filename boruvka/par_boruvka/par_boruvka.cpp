#include "mpi.h"
#include "../../matrix_input/mmio.h"
#include <stdio.h>
#include <bits/stdc++.h>
#include <unistd.h>
#include <tuple>
#include <unistd.h>
#include <fstream>

//#define use_integers

#ifdef use_integers
#define mat_t int
#define mpi_mat_t MPI_INT
#define readin(argc, argv) mtx_to_graph(argc, argv)
#define MAX_VALUE INT_MAX
#else
#define mat_t double  
#define mpi_mat_t MPI_DOUBLE
#define readin(argc, argv) mtx_to_graph_double(argc, argv)
#define MAX_VALUE DBL_MAX
#endif

struct subset{
    int parent;
    int rank;
};

struct helper_edge{
    int src;
    int dest;
    mat_t weight;

};

struct edgeComp{
	bool operator()( const helper_edge& lhs, const helper_edge& rhs) {
		if(lhs.weight == rhs.weight){
			if(lhs.src == rhs.src){
				return lhs.dest < rhs.dest;
			}
			else{
                return lhs.src < rhs.src;
            }
		}
		else return lhs.weight < rhs.weight;
	}
};

void smallestEdgeFunction(void *in, void *inout, int *len, MPI_Datatype *dptr){
    helper_edge *a = (helper_edge*) in;
    helper_edge *b = (helper_edge*) inout;

    helper_edge temp;
    for(int i = 0; i < *len; i++){
        if(a->weight == b->weight){
            temp.weight = a->weight;
			if(a->src == b->src){
			    temp.src = a->src;
                temp.dest = std::min(a->dest,b->dest);
			}
			else{
                if(a->src < b->src){
                    temp.src = a->src;
                    temp.dest = a->dest;
                }
                else{
                    temp.src = b->src;
                    temp.dest = b->dest;
                }
            }
		}
		else{
            if(a->weight < b->weight){
                temp = *a;
            }
            else{
                temp = *b;
            }
        }
        *b = temp;
        a++, b++;
    }
}

// find the set of element i
int find(struct subset subsets[], int i){
    if(subsets[i].parent != i){
        subsets[i].parent = find(subsets, subsets[i].parent);
    }
    return subsets[i].parent;
}

// a function to union two sets
void Union(struct subset subsets[], int x, int y, int *numSets){
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);

    // Attach smaller rank tree under root of high 
    if(subsets[xroot].rank < subsets[yroot].rank){
        subsets[xroot].parent = yroot; 
    }
    else if(subsets[xroot].rank > subsets[yroot].rank){
        subsets[yroot].parent = xroot;
    }
    // If ranks are same, then make one as root and increment its rank by one
    else {
        if (xroot < yroot){
            subsets[yroot].parent = xroot; 
            ++subsets[xroot].rank;
        }
        else{
            subsets[xroot].parent = yroot;
            ++subsets[yroot].rank;
        }
    }
    --(*numSets);
}

int main(int argc, char *argv[]){

    MPI_Init(&argc, &argv);
    int size, p_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &p_rank);
    double time = 0.0;
    if(p_rank == 0){
    	time = -MPI_Wtime();
    }

    //MPI Datatype for edges
    MPI_Datatype MPI_edge;
    const int block_length[3] = {1, 1, 1};
    const MPI_Aint displacement[3] = {offsetof(helper_edge, src), offsetof(helper_edge, dest), offsetof(helper_edge, weight)};
    const MPI_Datatype types[3] = { MPI_INT, MPI_INT, mpi_mat_t};
    MPI_Type_create_struct(3, block_length, displacement, types, &MPI_edge);
    MPI_Type_commit(&MPI_edge);

    // make new MPI operator to compare which edge
	MPI_Op smallestEdge;
	MPI_Op_create(smallestEdgeFunction, true, &smallestEdge);

    // get graph from matrix
    Graph<mat_t> graph = readin(argc, argv);

    // total number of vertices
    int V = mtx_numVertices(argc, argv);
    // total number of Edges
    // int E = mtx_numNNZ(argc, argv);

    //initialize MST
    Graph<mat_t> MST(V);

    mat_t MSTweight = 0;
    int edgesMST = 0;
    
    int numEdges = 0;

    // Allocate memory
    int numSets = V;
    struct subset *subsets = new struct subset[V];

    // An array to store cheapest edge of subset local and global
    helper_edge *cheapest = new helper_edge[V];
    helper_edge *global_cheapest = new helper_edge[V];

    // Create V subsets with single elements
    for (int v = 0; v < V; ++v) 
    { 
        subsets[v].parent = v; 
        subsets[v].rank = 0; 
    }

    // vector with all edges
    std::vector<helper_edge> edges;
    helper_edge e;
    
    if(p_rank == 0){
        for(int i = 0; i < V; i++){
            for(int j = i; j < V; j++){
                if(graph.valEdge(i,j) != 0){
                    e.src = i;
                    e.dest = j;
                    e.weight = graph.valEdge(i,j);
                    edges.push_back(e);
                    ++numEdges;
                }
            }
        }
    }
    // if(p_rank == 0){
    //     time = -MPI_Wtime();
    // }

    MPI_Bcast(&numEdges, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&V, 1, MPI_INT, 0, MPI_COMM_WORLD);

    //calculate how many edges need to be on one processor
    int edgesPart = numEdges / size;
    //calculate how many processors need to have one edge more to distribute all edges
    int ProPlOne = numEdges % size;

    // array with number of edges for each processor
    int *sendcounts = new int[size];
    // array with distance to first element for each processor
    int *displs = new int[size];
    
    //initialize sendcounts and distance on every processor
    for(int i = 0; i < size; ++i){
        if(i < ProPlOne){
            sendcounts[i] = edgesPart+1;
            displs[i] = i * (edgesPart+1);
        }
        else {
            sendcounts[i] = edgesPart;
            displs[i] = ProPlOne * (edgesPart+1) + (i-ProPlOne) * edgesPart;
        }
    }

    //initialize edgesPart to the right number in every processor
    for(int i = 0; i < ProPlOne; ++i){
        if(p_rank == i){
            edgesPart += 1;
        }
    }

    // initialize storage for edges on each processor
    helper_edge *edgesListPart = new helper_edge[edgesPart];

    // wait for all processors before distributing the edges
    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Scatterv(edges.data(), sendcounts, displs, MPI_edge, edgesListPart, edgesPart, MPI_edge, 0, MPI_COMM_WORLD);

    // Edge for initialization
    helper_edge max;

    MPI_Barrier(MPI_COMM_WORLD);

    // Loop until there is only one tree left
    for(int k = 1; k <= V && edgesMST < V - 1; k*=2){
        
        //initialize maximal edge
        max.dest = INT_MAX;
        max.src = INT_MAX;
        max.weight = MAX_VALUE;
        
        // Initialize cheapest array
        for(int v = 0; v < V; ++v){
            cheapest[v] = max;
            global_cheapest[v] = max;
        }

        // Traverse through all edges and update cheapest of every component
        for (int i=0; i < edgesPart; i++){
            // Find components (or sets) of two corners of current edge
            int set1 = find(subsets, edgesListPart[i].src);
            int set2 = find(subsets, edgesListPart[i].dest);
            
            if(set1 == set2)
                continue;
            else{
                if (cheapest[set1].weight > edgesListPart[i].weight){
                    cheapest[set1] = edgesListPart[i];
                }
                // sync between cheapest[set1] and cheapest[set2] (not yet verified if needed)
                if (cheapest[set2].weight > edgesListPart[i].weight)
                    cheapest[set2] = edgesListPart[i];
            }
        }

        // wait for all processors before finding the global cheapest edge for each vertex
        MPI_Barrier(MPI_COMM_WORLD);

        // find the cheapest edge for every vertex
        MPI_Allreduce(cheapest, global_cheapest, V, MPI_edge, smallestEdge, MPI_COMM_WORLD);

        // Consider the above picked cheapest edges and add them to MST
        for (int i = 0; i < V; ++i){

            if (global_cheapest[i].weight != MAX_VALUE){
                // find sets of the two vertices connected by the edge
                int set1 = find(subsets, global_cheapest[i].src ); 
                int set2 = find(subsets, global_cheapest[i].dest);

                // if they are in the same set, do not add the edge, otherwise there will be a loop
                if (set1 == set2){
                    continue;
                }

                // if they are in the same set, let the root processor add the edge to the MST
                if(p_rank == 0){
                    MSTweight += global_cheapest[i].weight;
                    //std::cout << global_cheapest[i].weight << std::endl;
                    MST.addEdge(global_cheapest[i].src, global_cheapest[i].dest, global_cheapest[i].weight);
                    // printf("Edge %d-%d included in MST\n", global_cheapest[i].src, global_cheapest[i].dest);
                }
                    edgesMST++;
                    // Join the two sets of the now connected vertices
                    Union(subsets, set1, set2, &numSets);
            }
        }     

    }

    if(p_rank == 0){
        std::cout << "MSTWeight = " << MSTweight << std::endl;
        time += MPI_Wtime();
        cout << "Time: "<< time << endl;
        ofstream timefile;
        timefile.open("times.txt", ios::app);
        timefile << size<< " "<<time << endl;
        timefile.close();
    }
    
    delete[] subsets;
    delete[] cheapest;
    delete[] global_cheapest;
    delete[] sendcounts;
    delete[] displs;
    delete[] edgesListPart;

    MPI_Finalize();


    return 0;
}
