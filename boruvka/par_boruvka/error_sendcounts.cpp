#include <iostream>
#include <string>

int main()
{
    int numEdges = 13;
    int size = 3;
    for(int rank = 0; rank < size; rank++){
        std::cout << "RANK: " << rank << std::endl;   
  
        int edgesPart = (numEdges + size - 1) / size;
        std::cout << "edgesPart: " << edgesPart << std::endl;
        if(rank == size - 1 && numEdges % edgesPart != 0){
            edgesPart = numEdges % edgesPart;   //probably memoryleak for last processor
        }
        std::cout << "edgesPart neu: " << edgesPart << std::endl;
        
    
        int *sendcounts = new int[size];
        for (int i = 0; i < size-1; ++i){
            sendcounts[i] = edgesPart;
        }
        if(numEdges%edgesPart != 0){
            sendcounts[size-1] = numEdges % edgesPart;
        }
        else{
            sendcounts[size-1] = edgesPart;
        }

        for(int i = 0; i < size; i++){
            std::cout << sendcounts[i] << " - " ;
        }
        std::cout << std::endl <<  "------------------------" << std::endl;
    }
    
}