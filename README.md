# DPHPC_project

## Sources 
https://github.com/elahehrashedi/MPI_Prime_MST  //MPI  
https://github.com/abarankab/parallel-boruvka   //  
https://github.com/marcindziadus/parallel-mst   //Cuda  
https://github.com/deXetrous/ParallelMst        //OpenMP  
https://github.com/Algue-Rythme/MST_MPI         //MPI in C  
https://github.com/nikitawani07/MST-Parallel.   //MPI
  
https://kfuh1.github.io/15418-project/  


Meetings
- [ ] 12.11.2020 10:00

Tasks
- [ ] Alle neuen Tasks hier hinschreiben

Alain
- [ ] Dense graphs suchen
- [ ] Boruvka pralell

Anna
- [ ] Adjacency list
- [ ] Prim sequentiell

Beni
- [ ] Matrix zu adjacency list
- [ ] Prim parallel

Zuzanna
- [ ] Dense graphs suchen
- [ ] Zuzanna kruskal parallel



https://sparse.tamu.edu/  //Florida Sparse Matrix Collection
-> Wenn ihr Matizen dort sucht, benutzt den Filter
 "Structure and Entry Types" und dann wählt bei Type int oder real 
 (nicht complex oder binary) und bei structure 
 Symmetric (für ungerichtete Graphen)
