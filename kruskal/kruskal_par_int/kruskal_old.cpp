#include "../../matrix_input/mmio.h"
#include <mpi.h>
#include <iostream>
#include <bits/stdc++.h>
#include <vector>

int find(std::vector<int> v, int i){
  while(v[i]!=i){
    i=v[i];
  }
  return i;
}

void uni(int i, int j, std::vector<int>& v){
  int a=find(v, i);
  int b=find(v, j);

  v[a]=b;
}

int main(int argc, char *argv[]){

  MPI_Init(&argc, &argv);
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  Graph<int> g= mtx_to_graph(argc, argv);

  //g.printGraph();

  Graph<int> mst;
  int numVer=g.numVertices();
  int edgecount=0;

  int countVer=numVer/size;
  int end=(rank+1)*countVer;

  if(rank==size-1){
    end=numVer-1;
  }

  std::vector<int>vec (numVer+1);
  for(int i=0; i<vec.size(); ++i){
    vec[i]=i;
  }
  while(edgecount<(numVer-1)){

    int min=INT_MAX;
    int a=-1;
    int b=-1;

    for(int i=rank*countVer; i < end; ++i){
      for(int j=i+1; j<numVer; ++j){
        if(g.vertexExists(i)&&g.vertexExists(j)){
          if(g.valEdge(i, j)){

            int weight=g.valEdge(i, j);
            if(weight < min && (find(vec, i)!=find(vec, j))){
              min=weight;
              a=i;
              b=j;
            }
          }
        }
      }
    }
    int choice[3]={ a, b, min};
    //send
    if(rank!= 0){
      MPI_Send(choice, 3, MPI_INT, 0, edgecount, MPI_COMM_WORLD);
    }
    //rank 0 wählt aus
    else{
      int choice1[3];
      for(int k=1; k<size; ++k){
        MPI_Recv(choice1, 3, MPI_INT, k, edgecount, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if(choice1[2]<choice[2]){
          choice[0]=choice1[0];
          choice[1]=choice1[1];
          choice[2]=choice1[2];
        }
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(choice, 3, MPI_INT, 0, MPI_COMM_WORLD);

    a=choice[0];
    b=choice[1];
    min=choice[2];

    //alle bekommen edge und adden sie
    if((a!=-1)&& !mst.vertexExists(a)){

      //std::cout<<"a="<<a<<std::endl;
      mst.addVertex(a);
    }

    if((b!=-1)&& !mst.vertexExists(b)){

      //std::cout<<"b="<<b<<std::endl;
      mst.addVertex(b);
    }
    mst.addEdge(a, b, min);
    uni(a, b, vec);
    g.removeEdge(a, b);
    ++edgecount;
  }
  if(rank==0){
    mst.printGraph();
  }
  MPI_Finalize();
  return 0;
}
