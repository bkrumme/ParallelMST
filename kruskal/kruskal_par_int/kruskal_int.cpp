#include "../../matrix_input/kruskmmio.h"
#include <mpi.h>
#include <iostream>
#include <bits/stdc++.h>
#include <vector>
#include <fstream>

int find(std::vector<int> v, int i){
  while(v[i]!=i){
    i=v[i];
  }
  return i;
}

void uni(int i, int j, std::vector<int>& v){
  int a=find(v, i);
  int b=find(v, j);

  v[a]=b;
}

int main(int argc, char *argv[]){

  MPI_Init(&argc, &argv);
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  double time=0.0;
  if(rank == 0){
    time = -MPI_Wtime();
  }
  Graph<int> g= mtx_to_graph(argc, argv);

  //g.printGraph();

  int numVer=g.numVertices();
  Graph<int> mst(numVer);
  int edgecount=0;
  int edgesWeight = 0;

  int countVer=numVer/size;
  int end=(rank+1)*countVer;

  if(rank==size-1){
    end=numVer-1;
  }

  std::vector<int> root (numVer+1);
  for(int i=0; i<root.size(); ++i){
    root[i]=i;
  }


  while(edgecount<(numVer-1)){

    int min=INT_MAX;
    int a=-1;
    int b=-1;

    bool smaller = true;
    for(int i=rank*countVer; i < end; ++i){
      smaller = true;
      for(auto it=g.vertexMapBegin(i);smaller == true && it!=g.vertexMapEnd(i); ++it){
        int j=it->second;
        int weight=it->first;
        if(weight > min){
          smaller = false;
          //break;
        }
        else{
          if(find(root, i)!=find(root, j)){
              min=weight;
              a=i;
              b=j;
              smaller = false;
          }
          else{
             g.removeSingleEdge(i,j);
          }
        }
      }
    }
    int choice[3]={ a, b, min};
    //send
    if(rank!= 0){
      MPI_Send(choice, 3, MPI_INT, 0, edgecount, MPI_COMM_WORLD);
    }
    //rank 0 wählt aus
    else{
      int choice1[3];
      for(int k=1; k<size; ++k){
        MPI_Recv(choice1, 3, MPI_INT, k, edgecount, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if(choice1[2]<choice[2]){
          choice[0]=choice1[0];
          choice[1]=choice1[1];
          choice[2]=choice1[2];
        }
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(choice, 3, MPI_INT, 0, MPI_COMM_WORLD);

    a=choice[0];
    b=choice[1];
    min=choice[2];

    if(a != -1 && b != -1 && min != INT_MAX){
      mst.addEdge(a, b, min);
      edgesWeight += min;
      uni(a, b, root);
      g.removeEdge(a, b);
    }
    ++edgecount;
  }
  if(rank==0){ 
    std::cout << edgesWeight << std::endl;
    time += MPI_Wtime();
    cout << "Time: "<< time <<endl;
    ofstream timefile;
    timefile.open("times.txt", ios::app);
    timefile << size<< " "<<time <<endl;
    timefile.close();
  }
  MPI_Finalize();
  return 0;
}
