for i in 1 2 3 4 5
do
	mpirun -np 1 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 4 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 8 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 12 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 16 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 20 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 24 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 28 ./kruskal ../../matrix_input/football/football.mtx
	mpirun -np 32 ./kruskal ../../matrix_input/football/football.mtx
done