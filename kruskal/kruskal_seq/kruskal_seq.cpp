#include "../matrix_input/mmio.h"
#include <iostream>
#include <bits/stdc++.h>
#include <vector>

int find(std::vector<int> v, int i){
  while(v[i]!=i){
    i=v[i];
  }
  return i;
}

void uni(int i, int j, std::vector<int>& v){
  int a=find(v, i);
  int b=find(v, j);

  v[a]=b;
}

int main(int argc, char *argv[]){
  int size, rank;
  Graph g= mtx_to_graph(argc, argv);

  g.printGraph();

  Graph mst;
  int numVer=g.numVertices();
  int edgecount=0;

  std::vector<int>vec (numVer+1);
  for(int i=0; i<vec.size(); ++i){
    vec[i]=i;
  }
  while(edgecount<(numVer-1)){

    //mst.printGraph();
    //std::cout<<"counter="<<edgecount<<std::endl;
    int min=INT_MAX;
    int a=-1;
    int b=-1;

    for(int i=0; i < numVer-1; ++i){
      for(int j=i+1; j<numVer; ++j){
        if(g.vertexExists(i)&&g.vertexExists(j)){
          if(g.edgeExists(i, j)){

            int weight=g.valEdge(i, j);
            if(weight < min && (find(vec, i)!=find(vec, j))){
              min=weight;
              a=i;
              b=j;
            }
          }
        }
      }
    }

    if((a!=-1)&& !mst.vertexExists(a)){

      //std::cout<<"a="<<a<<std::endl;
      mst.addVertex(a);
    }
    mst.printGraph();
    //std::cout<<"b="<<b<<std::endl;
    std::cout<<mst.vertexExists(b)<<std::endl;

    //mst.printGraph();
    if((b!=-1)&& !mst.vertexExists(b)){

      //std::cout<<"b="<<b<<std::endl;
      mst.addVertex(b);
    }
    uni(a, b, vec);
    mst.addEdge(a, b, min);
    g.removeEdge(a, b);
    ++edgecount;
  }

  mst.printGraph();
  return 0;
}
