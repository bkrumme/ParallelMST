import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

data_bor = open("results_boruvka/Reuters911_128.txt", 'r')
data_prim = open("prim_Reuters911_result.txt", 'r')
data_krus = open("../kruskal/kruskal_par_int/Reuters911_times.txt", 'r')
max_processors = 32

# from 2 to [max_processors] processors, in steps of 2
num_processor_cores = range(2,max_processors+1,2)

raw_bor = []
raw_bor = [line.split() for line in data_bor]

raw_prim = []
raw_prim = [line.split() for line in data_prim]

raw_krus = []
raw_krus = [line.split() for line in data_krus]


seq_time_bor = 0
seq_time_prim = 0
seq_time_krus = 0
measurements = 5

for i in raw_bor:
    if i[0] == '1' :
        i[1] = float(i[1])
        seq_time_bor += i[1]

seq_time_bor /= measurements

for i in raw_prim:
    if i[0] == '2' :
        i[1] = float(i[1])
        seq_time_prim += i[1]

seq_time_prim /= measurements

for i in raw_krus:
    if i[0] == '1' :
        i[1] = float(i[1])
        seq_time_krus += i[1]

seq_time_krus /= measurements

processes = np.arange(0, max_processors+1, 4)
processes[0] = 2
averages_bor = np.zeros(len(processes), float)
speedup_bor = np.zeros(len(processes), float)
averages_prim = np.zeros(len(processes), float)
speedup_prim = np.zeros(len(processes), float)
averages_krus = np.zeros(len(processes), float)
speedup_krus = np.zeros(len(processes), float)

for i in processes:
    for j in raw_bor:
        if j[0] == str(i):
            j[1] = float(j[1])
            averages_bor[i/4] += j[1]
    averages_bor[i/4] /= 5

speedup_bor = seq_time_bor / averages_bor
print(speedup_bor)

for i in processes:
    for j in raw_prim:
        if j[0] == str(i):
            j[1] = float(j[1])
            averages_prim[i/4] += j[1]
    averages_prim[i/4] /= 5

speedup_prim = seq_time_prim / averages_prim
print(speedup_prim)

for i in processes:
    for j in raw_krus:
        if j[0] == str(i):
            j[1] = float(j[1])
            averages_krus[i/4] += j[1]
    averages_krus[i/4] /= 5

speedup_krus = seq_time_krus / averages_krus
print(seq_time_krus)

f = interp1d(processes, speedup_prim, kind='cubic')

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(processes, speedup_bor, label='Boruvka')
ax.plot(processes, speedup_prim, label='Prim')
ax.plot(processes, speedup_krus, label='Kruskal')
plt.yscale('log')
plt.xlabel("#processes")
plt.ylabel("speedup")


plt.legend(loc=2)
plt.show()
plt.savefig('speedup_reuters.eps', format='eps')