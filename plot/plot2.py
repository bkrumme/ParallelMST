import numpy as np
import matplotlib.pyplot as plt

#data input
data_prim = open("prim_Reuters911_result.txt", 'r')
raw_prim = []
raw_prim = [line.split() for line in data_prim]
data_bor= open("results_boruvka/Reuters911_128.txt", 'r')
raw_bor = []
raw_bor = [line.split() for line in data_bor]
data_krus = open("../kruskal/kruskal_par_int/Reuters911_times.txt", 'r')
raw_krus = []
raw_krus = [line.split() for line in data_krus]

data_boost_prim = open("prim_boost_bccstk35.txt", 'r')
raw_boost_prim = [line.split() for line in data_boost_prim]
data_boost_krus = open("kruskal_boost_bcsstk35.txt", 'r')
raw_boost_krus = [line.split() for line in data_boost_krus]



avg_boost_prim = 0
min_boost_prim = float(raw_boost_prim[0][0])
max_boost_prim = float(raw_boost_prim[0][0])
print(raw_boost_prim)
for i in raw_boost_prim:
	avg_boost_prim += float(i[0])
	min_boost_prim = min(min_boost_prim, float(i[0]))
	max_boost_prim = max(max_boost_prim, float(i[0]))
avg_boost_prim /= 5

avg_boost_krus = 0
min_boost_krus = float(raw_boost_krus[0][0])
max_boost_krus = float(raw_boost_krus[0][0])
print(raw_boost_krus)
for i in raw_boost_krus:
	avg_boost_krus += float(i[0])
	min_boost_krus = min(min_boost_krus, float(i[0]))
	max_boost_krus = max(max_boost_krus, float(i[0]))
avg_boost_krus /= 5

boost_prim_avg_l = [avg_boost_prim, avg_boost_prim]
boost_prim_min_l = [min_boost_prim, min_boost_prim]
boost_prim_max_l = [max_boost_prim, max_boost_prim]

boost_krus_avg_l = [avg_boost_krus, avg_boost_krus]
boost_krus_min_l = [min_boost_krus, min_boost_krus]
boost_krus_max_l = [max_boost_krus, max_boost_krus]
boost_axis = [1,32]


# avg_boost_kruskal = 0

# for i in raw_boost_prim:
# 	avg_boost_prim += i

# for i in raw_boost_krus:
# 	avg_boost_krus += i

# avg_boost_prim /= len(avg_boost_prim)
# avg_boost_krus /= len(avg_boost_krus)



for i in raw_prim:
	i[0] = int(i[0])
	i[1] = float(i[1])
	del i[2]

for i in raw_bor:
	i[0] = int(i[0])
	i[1] = float(i[1])

for i in raw_krus:
	i[0] = int(i[0])
	i[1] = float(i[1])



# for pdb1HYS, include this line ( the file contains also other measurements, but only use the one from pdb1HYS)
#raw_data = list(filter(lambda a: a[2] == "../matrix_input/pdb1HYS/pdb1HYS.mtx" ,raw_data))



# graph number of cores plotted
max_processors = 32
processor_cores = range(0,max_processors+1,4)
processor_cores[0] = 1
print(processor_cores)


# plot_data[i][0] is the number of processes
# plot_data[i][j] (j = 1 to 5) is the time for the number of processes 
plot_prim = []
for i in processor_cores:
	temp = []
	temp.append(i)
	plot_prim.append(temp)

plot_bor = []
for i in processor_cores:
	temp = []
	temp.append(i)
	plot_bor.append(temp)

plot_krus = []
for i in processor_cores:
	temp = []
	temp.append(i)
	plot_krus.append(temp)

for i in raw_prim:
	if(i[0] in processor_cores):
		index = processor_cores.index(i[0])
#		print(index)
#		print(i[0])
#		print("---------")
		plot_prim[index].append(i[1])

for i in raw_bor:
	if(i[0] in processor_cores):
		index = processor_cores.index(i[0])
#		print(index)
#		print(i[0])
#		print("---------")
		plot_bor[index].append(i[1])

for i in raw_krus:
	if(i[0] in processor_cores):
		index = processor_cores.index(i[0])
#		print(index)
#		print(i[0])
#		print("---------")
		plot_krus[index].append(i[1])


#print(plot_data)
#print(".------------------.")

num_measurements = 5
# x axis
xdata = np.array(processor_cores)

# compute average
avg_prim = [0.0] * len(processor_cores)
for i in range(len(avg_prim)):
	for j in range(num_measurements):
		avg_prim[i] += plot_prim[i][j+1]
	avg_prim[i] /= num_measurements
avgdata_prim = np.array(avg_prim)

avg_bor = [0.0] * len(processor_cores)
for i in range(len(avg_bor)):
	for j in range(num_measurements):
		avg_bor[i] += plot_bor[i][j+1]
	avg_bor[i] /= num_measurements
avgdata_bor = np.array(avg_bor)

avg_krus = [0.0] * len(processor_cores)
for i in range(len(avg_krus)):
	for j in range(num_measurements):
		avg_krus[i] += plot_krus[i][j+1]
	avg_krus[i] /= num_measurements
avgdata_krus = np.array(avg_krus)

# compute minima
mins_prim = []
for i in plot_prim:
	mins_prim.append(i[1])
for i in range(len(mins_prim)):
	for j in range(num_measurements-1):
		mins_prim[i] = min(mins_prim[i], plot_prim[i][j+2])
mindata_prim = np.array(mins_prim)

mins_bor = []
for i in plot_bor:
	mins_bor.append(i[1])
for i in range(len(mins_bor)):
	for j in range(num_measurements-1):
		mins_bor[i] = min(mins_bor[i], plot_bor[i][j+2])
mindata_bor = np.array(mins_bor)

mins_krus = []
for i in plot_krus:
	mins_krus.append(i[1])
for i in range(len(mins_krus)):
	for j in range(num_measurements-1):
		mins_krus[i] = min(mins_krus[i], plot_krus[i][j+2])
mindata_krus = np.array(mins_krus)

# compute maxima
maxs_prim = []
for i in plot_prim:
	maxs_prim.append(i[1])
for i in range(len(maxs_prim)):
	for j in range(num_measurements-1):
		maxs_prim[i] = max(maxs_prim[i], plot_prim[i][j+2])
maxdata_prim = np.array(maxs_prim)

maxs_bor = []
for i in plot_bor:
	maxs_bor.append(i[1])
for i in range(len(maxs_bor)):
	for j in range(num_measurements-1):
		maxs_bor[i] = max(maxs_bor[i], plot_bor[i][j+2])
maxdata_bor = np.array(maxs_bor)

maxs_krus = []
for i in plot_krus:
	maxs_krus.append(i[1])
for i in range(len(maxs_krus)):
	for j in range(num_measurements-1):
		maxs_krus[i] = max(maxs_krus[i], plot_krus[i][j+2])
maxdata_krus = np.array(maxs_krus)



#boost_prim_avg_l = [avg_boost_prim, avg_boost_prim]
#boost_krus_avg_l = [avg_boost_krus, avg_boost_krus]
#boost_axis = [1,32]

#print(xdata)
#print(avgdata)
#print(mindata)

#print(maxdata)

plt.rcParams.update({'font.size': 11})

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(xdata, avgdata_prim, label='Prim')
ax.plot(xdata, avgdata_bor, label='Boruvka')
ax.plot(xdata, avgdata_krus, label='Kruskal')
ax.plot(boost_axis, boost_prim_avg_l, label='boost::prim')
ax.plot(boost_axis, boost_krus_avg_l, label='boost::kruskal')
# ax.plot(xdata, avg_boost_prim, label='boost_prim')
# ax.plot(xdata, avg_boost_krus, label='boost_krus')

#.plot(xdata, avgdata_prim, label='prim', xdata, avgdata_bor, label='boruvka')
plt.yscale('log')
plt.xlabel("#processes")
plt.ylabel("time in [s]")
#plt.plot(xdata, mindata)
#plt.plot(xdata, maxdata)
plt.fill_between(xdata, mindata_prim, maxdata_prim, color='blue', alpha=0.2)
plt.fill_between(xdata, mindata_bor, maxdata_bor, color='orange', alpha=0.2)
plt.fill_between(xdata, mindata_krus, maxdata_krus, color='green', alpha=0.2)
plt.fill_between(boost_axis, boost_prim_min_l, boost_prim_max_l, color='red', alpha=0.2)
plt.fill_between(boost_axis, boost_krus_min_l, boost_krus_max_l, color='violet', alpha=0.2)

plt.legend()
plt.show()
plt.savefig('time_reuters.eps', format='eps')