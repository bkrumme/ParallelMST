import numpy as np
import matplotlib.pyplot as plt



data_file = open("bcsstk35_times.txt", 'r')


max_processors = 64

# from 2 to [max_processors] processors, in steps of 2
num_processor_cores = range(2,max_processors+1,2)

raw_data = []
raw_data = [line.split() for line in data_file]


#print(num_processor_cores)

for i in raw_data:
	i[0] = int(i[0])
	i[1] = float(i[1])
	#del i[2] #line only needed if input filename is in third column

#exclude all data from with num_processes > max_processors
cleared_data = list(filter(lambda a: a[0] <= max_processors, raw_data))

data = np.array(cleared_data)


plt.xticks(num_processor_cores)
plt.scatter(data[:,0], data[:,1],marker="_")
plt.ylabel('time in s')
plt.xlabel('number of processors')

# Change Title
plt.title("Prim Parallel: bcsstk35 (30.237 Knoten, 1.450.163 Kanten)")

plt.show()