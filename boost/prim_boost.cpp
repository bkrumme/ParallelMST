#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/graph_utility.hpp>

#include <iostream>
#include <fstream>
#include <chrono>

#include "../matrix_input/mmio.h"

//uncomment next line to use doubles
//#define use_integers

#ifdef use_integers
#define mat_t int
#define readin(argc, argv) mtx_to_boostgraph(argc, argv)
#define triplet input_triplet

#else
#define mat_t double  
#define readin(argc, argv) mtx_to_boostgraph_double(argc, argv)
#define triplet input_triplet_double
#endif



int main(int argc, char *argv[]) {

  auto start = std::chrono::high_resolution_clock::now();

    using Graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, boost::no_property, 
          boost::property<boost::edge_weight_t, mat_t>>;

    // read edges and weights into arrays
    std::vector<triplet> input = readin(argc, argv);

    unsigned highest_vertex = 0;
    for (auto t: input)
        highest_vertex = std::max({highest_vertex, t.source, t.target});
    unsigned num_vertices = highest_vertex + 1;
    Graph G(num_vertices);

    for (auto t: input) {
        assert(t.source < num_vertices && "numvertices source");
        assert(t.target < num_vertices && "numvertices target");

        // add edge (no self-cylces!!!)
        if(t.source != t.target){
            add_edge(t.source, t.target, t.weight, G);
        }
    }

    //create parent vector to store solution
    std::vector<Graph::vertex_descriptor> parents(num_vertices);

    //run prim
    prim_minimum_spanning_tree(G, parents.data());

    boost::property_map<Graph, boost::edge_weight_t>::type weight = get(boost::edge_weight, G);

    mat_t sum = 0;
    for (std::size_t i = 0; i < num_vertices; ++i){
      if(parents[i] != i){
        assert(edge(parents[i],i,G).second  && "edge doesn't exist :(");
        sum += get(weight, edge(parents[i], i, G).first);        
      }
    }

    std::cout << "total sum = " << sum << std::endl;

    auto stop = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    std::cout << duration.count()/1000000. << std::endl;

    ofstream timefile;
    timefile.open("times.txt", ios::app);
    timefile << duration.count()/1000000. << endl;
    timefile.close();

}