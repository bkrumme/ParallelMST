#!/bin/bash

for i in 1 2 3 4 5
do
	mpirun -np 1 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 4 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 8 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 12 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 16 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 20 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 24 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 28 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 32 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 36 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 40 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 44 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 48 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 52 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 56 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 60 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 64 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 68 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 72 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 76 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 80 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 84 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 88 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 92 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 96 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 100 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 104 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 108 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 112 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 116 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 120 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 124 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 128 ./kruskal_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
done
