#!/bin/bash

for i in 1 2 3 4 5
do
	mpirun -np 1 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 4 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 8 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 12 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 16 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 20 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 24 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 28 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 32 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 36 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 40 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 44 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 48 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 52 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 56 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 60 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 64 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 68 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 72 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 76 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 80 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 84 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 88 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 92 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 96 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 100 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 104 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 108 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 112 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 116 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 120 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 124 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
	mpirun -np 128 ./prim_boost ../../matrix_input/bcsstk35/bcsstk35.mtx
done
